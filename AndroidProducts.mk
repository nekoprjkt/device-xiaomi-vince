#
# Copyright (C) 2021 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/aosp_vince.mk

COMMON_LUNCH_CHOICES := \
    aosp_vince-user \
    aosp_vince-userdebug \
    aosp_vince-eng
